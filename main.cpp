#include <QApplication>

#include <QTimer>

#include <QWidget>

#include <QHBoxLayout>
#include <QToolBar>
#include <QAction>
#include <QIcon>

#include <QBitArray>

#include <QList>

#include <QGraphicsScene>
#include <QGraphicsView>

#include <QGraphicsRectItem>
#include <QGraphicsEllipseItem>
#include <QGraphicsTextItem>

#include <QKeyEvent>


class SnakeWidget: public QWidget
{
    Q_OBJECT
public:
    explicit SnakeWidget(QWidget* parent=0);

protected:
    virtual void keyPressEvent(QKeyEvent*);

private slots:

    void start();
    void pause();
    void stop();

    void onTimerTick();

private:

    struct State
    {
        enum state_type {None, Steady, Running, Paused, GameOver, UserWin};
    };

    typedef State::state_type state_type;


    struct Move
    {
        enum move_dir_type {Up, Down, Left, Right};

        static QPointF moveVector(move_dir_type move)
        {
            switch (move)
            {
                case Up:
                    return QPointF(0, -1);

                case Down:
                    return QPointF(0, 1);

                case Left:
                    return QPointF(-1, 0);

                case Right:
                    return QPointF(1, 0);

                default:
                    return QPointF(0, 0);
            }
        }
    };

    typedef Move::move_dir_type move_dir_type;


    enum {
        TileSize=8, FontSize=36,
        SnakeInitialLength=3,
        SnakeMemberSize=TileSize,
        Tick=300,
        GameFieldSquareSide=40, CoinCount=10
    };

    void setUpToolBar();
    void setUpGraphicsView();
    void setUpGameField();
    void setUpTextItem();
    void setUpSnake();
    void setUpCoins();
    void setUpTimer();

    bool takeCoin();

    void goState(state_type);

    void updateTimerState();
    void updateToolBarState();
    void updateTextItemState();

    void display(const QString&);

    QAbstractGraphicsShapeItem* makeSnakeMember(const QPointF&);

    state_type state_;

    move_dir_type move_dir_;

    QTimer* timer_;

    QToolBar* tool_bar_;

    QAction *start_action_, *pause_action_, *stop_action_;

    QGraphicsScene* scene_;
    QGraphicsView* view_;

    QGraphicsRectItem* game_field_;

    QGraphicsTextItem* text_item_;

    QList<QAbstractGraphicsShapeItem*> snake_, coins_;
};


SnakeWidget::SnakeWidget(QWidget* parent):
    QWidget(parent),
    state_(State::None),
    move_dir_(Move::Up),
    timer_(0),
    tool_bar_(0), start_action_(0), pause_action_(0), stop_action_(0),
    scene_(0), view_(0),
    game_field_(0), text_item_(0)
{
    new QVBoxLayout(this);

    setFocusPolicy(Qt::StrongFocus);

    setUpToolBar();
    setUpGraphicsView();
    setUpGameField();
    setUpTextItem();
    setUpSnake();
    setUpCoins();
    setUpTimer();

    goState(State::Steady);
}

void SnakeWidget::setUpToolBar()
{
    tool_bar_ = new QToolBar(this);

    layout()->addWidget(tool_bar_);

    start_action_ = tool_bar_->
        addAction(QIcon(":/start.ico"), tr("start"), this, SLOT(start()));

    pause_action_ = tool_bar_->
        addAction(QIcon(":/pause.ico"), tr("pause"), this, SLOT(pause()));

    stop_action_ = tool_bar_->
        addAction(QIcon(":/stop.ico"), tr("stop"), this, SLOT(stop()));
}

void SnakeWidget::setUpGraphicsView()
{
    scene_ = new QGraphicsScene(this);
    view_ = new QGraphicsView(scene_, this);

    layout()->addWidget(view_);

    view_->setRenderHint(QPainter::Antialiasing);
    view_->setFocusPolicy(Qt::NoFocus);
}

void SnakeWidget::setUpGameField()
{
    int game_field_size = GameFieldSquareSide * TileSize;

    game_field_ = scene_->addRect(
        -game_field_size / 2, -game_field_size / 2,
        game_field_size, game_field_size
    );
}

void SnakeWidget::setUpTextItem()
{
    text_item_ = new QGraphicsTextItem(game_field_);

    text_item_->hide();

    QFont font = text_item_->font();

    font.setPointSize(FontSize);

    text_item_->setFont(font);
}

void SnakeWidget::setUpSnake()
{
    move_dir_ = Move::Up;

    qDeleteAll(snake_);
    snake_.clear();

    for (int i = 0; i < SnakeInitialLength; ++i)
        snake_.append(makeSnakeMember(QPointF(0, i * SnakeMemberSize)));
}

void SnakeWidget::setUpCoins()
{
    qDeleteAll(coins_);
    coins_.clear();

    const qreal
        w = game_field_->rect().width(),
        h = game_field_->rect().height();

    const int
        tile_per_row = (w / TileSize) - 2,
        tile_per_col = (h / TileSize) - 2;

    const int tile_count = tile_per_row * tile_per_col;

    QBitArray used(tile_count);

    const int outline = 3;

    for (int i = 0; i < CoinCount; ++i)
    {
        int random = qrand() % tile_count;

        int tile = -1;

        while (random >= 0)
        {
            tile = (tile + 1) % used.size();

            if (!used.at(tile))
                --random;
        }

        used.setBit(tile);

        qreal
            x = (tile % tile_per_row - tile_per_row / 2) * TileSize,
            y = (tile / tile_per_row - tile_per_col / 2) * TileSize;

        coins_.append(
            new QGraphicsRectItem(
                x - TileSize / 2 + outline, y - TileSize / 2 + outline,
                TileSize - 2 * outline, TileSize - 2 * outline,
                game_field_
            )
        );
    }
}

void SnakeWidget::setUpTimer()
{
    timer_ = new QTimer(this);

    timer_->setInterval(Tick);
    timer_->setSingleShot(false);

    connect(timer_, SIGNAL(timeout()), this, SLOT(onTimerTick()));
}

void SnakeWidget::start()
{
    goState(State::Running);
}

void SnakeWidget::pause()
{
    switch (state_)
    {
        case State::Running:
            goState(State::Paused);
            break;

        case State::Paused:
            goState(State::Running);
            break;

        default:
            break;
    }
}

void SnakeWidget::stop()
{
    goState(State::GameOver);
}

void SnakeWidget::onTimerTick()
{
    if (state_ == State::Running)
    {
        QPointF next_head_pos =
            snake_.first()->pos() + SnakeMemberSize * Move::moveVector(move_dir_);

        qreal augment = SnakeMemberSize / 2;

        if (
            !game_field_->rect()
                .adjusted(augment, augment, -augment, -augment)
                .contains(next_head_pos)
        )
            goState(State::GameOver);
        else
        {
            QPointF tail_pos = snake_.last()->pos();

            QAbstractGraphicsShapeItem* head = snake_.takeLast();

            head->setPos(next_head_pos);

            snake_.prepend(head);

            if (takeCoin())
            {
                snake_.append(makeSnakeMember(tail_pos));

                if (coins_.isEmpty())
                    goState(State::UserWin);
            }
        }
    }
}

void SnakeWidget::keyPressEvent(QKeyEvent* event)
{
    switch (event->key())
    {
        case Qt::Key_Left:
            move_dir_ = Move::Left;
            break;

        case Qt::Key_Right:
            move_dir_ = Move::Right;
            break;

        case Qt::Key_Up:
            move_dir_ = Move::Up;
            break;

        case Qt::Key_Down:
            move_dir_ = Move::Down;
            break;

        case Qt::Key_Space:
            switch (state_)
            {
                case State::Steady:
                case State::GameOver:
                case State::UserWin:
                    goState(State::Running);
                    break;

                case State::Running:
                    goState(State::Paused);
                    break;

                case State::Paused:
                    goState(State::Running);
                    break;

                default:
                    break;
            }
            break;

        case Qt::Key_Escape:
            switch (state_)
            {
                case State::Running:
                case State::Paused:
                    goState(State::GameOver);
                    break;

                default:
                    break;
            }
            break;

        default:
            QWidget::keyPressEvent(event);
            break;
    }
}

bool SnakeWidget::takeCoin()
{
    QAbstractGraphicsShapeItem* head = snake_.first();

    for (int i = 0; i < coins_.count(); ++i)
    {
        if (head->collidesWithItem(coins_[i]))
        {
            delete coins_.takeAt(i);

            return true;
        }
    }

    return false;
}

void SnakeWidget::goState(state_type state)
{
    if (state != state_)
    {
        if (state == State::Running)
        {
            switch (state_)
            {
                case State::GameOver:
                case State::UserWin:
                    setUpCoins();
                    setUpSnake();
                    break;

                default:
                    break;
            }
        }

        state_ = state;

        updateTimerState();
        updateToolBarState();
        updateTextItemState();
    }
}

void SnakeWidget::updateTimerState()
{
    if (state_ == State::Running)
        timer_->start();
    else
        timer_->stop();
}

void SnakeWidget::updateToolBarState()
{
    switch (state_)
    {
        case State::Running:
            start_action_->setEnabled(false);
            pause_action_->setEnabled(true);
            stop_action_->setEnabled(true);
            break;

        case State::Steady:
        case State::GameOver:
        case State::UserWin:
            start_action_->setEnabled(true);
            pause_action_->setEnabled(false);
            stop_action_->setEnabled(false);
            break;

        default:
            break;
    }
}

void SnakeWidget::updateTextItemState()
{
    switch (state_)
    {
        case State::Paused:
            display(tr("Paused"));
            break;

        case State::GameOver:
            display(tr("Game over"));
            break;

        case State::UserWin:
            display(tr("You win"));
            break;

        default:
            display(QString());
            break;
    }
}

void SnakeWidget::display(const QString& text)
{
    if (text.isEmpty())
        text_item_->hide();
    else
    {
        text_item_->setPlainText(text);

        QSizeF text_size = text_item_->boundingRect().size();

        text_item_->setPos(-text_size.width() / 2, -text_size.height() / 2);

        text_item_->show();
    }
}

QAbstractGraphicsShapeItem* SnakeWidget::makeSnakeMember(const QPointF& pos)
{
    QAbstractGraphicsShapeItem* result = new QGraphicsEllipseItem(
        -SnakeMemberSize / 2, -SnakeMemberSize / 2,
        SnakeMemberSize, SnakeMemberSize,
        game_field_
    );

    result->setPos(pos);

    return result;
}


int main(int argc, char* argv[])
{
    QApplication app(argc, argv);

    SnakeWidget snake_widget;

    snake_widget.show();

    return app.exec();
}


#include "main.moc"
