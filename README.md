# README / ЧАВО #

Test project for one small IT-company.

Тестовое задание, выполненное для одной небольшой компании.

### What is this repository for? / Что тут к чему ###

"Snake" simple game implemented on pure Qt in 4 hours (basically).

Игра "змейка", реализована на чистом Qt за 4 часа (в основной части).

### Contribution guidelines / Как бы помочь ###

Do nothing.

Просто не мешайте. Этот проект сделан "для себя", в чисто познавательных целях.
